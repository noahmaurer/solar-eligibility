//eligibility criteria set up as an object.
//each state is set up by its two character state abbreviation
//each state has a stateName which is their non-abbreviated name
//each state has household size as a key, and AMI threshold as its value
const stateIncomeThresholds = {
    MA: {
        stateName: "Massachusetts",
        1: 84280.0,
        2: 96320.0,
        3: 108360.0,
        4: 120400.0,
        5: 130032.0,
        6: 139664.0,
        7: 149296.0,
        8: 158928.0,
    },
    RI: {
        stateName: "Rhode Island",
        1: 69510.0,
        2: 79440.0,
        3: 89370.0,
        4: 99300.0,
        5: 107240.0,
        6: 115188.0,
        7: 123132.0,
        8: 131076.0,
    },
    TX: {
        stateName: "Texas",
        1: 59710.0,
        2: 68240.0,
        3: 76770.0,
        4: 85300.0,
        5: 92124.0,
        6: 98948.0,
        7: 105772.0,
        8: 112596.0,
    },
};

//state -> string (example: Rhode Island = RI), state length = 2
//householdSize -> int, householdSize > 1
//income -> float (example: $84,280.00 = 84,280.00), income >= 0
function solarEligibility(state, householdSize, income) {
    //created for edge cases where householdSize is > 8
    let householdSizeCopy = householdSize;
    if (householdSize > 8) {
        householdSizeCopy = 8;
    }
    //check for edge case where householdSize < 0
    else if (householdSize < 1) {
        return {
            elibible: false,
            message: `Error: household size must be greater than 0. Your input: ${householdSize}`,
        };
    }

    //check for edge case where user inputs an invalid state abbreviation code
    if (!(state in stateIncomeThresholds)) {
        return {
            elibible: false,
            message: `Error: Invalid state abbreviation code. Your input: ${state}`,
        };
    }

    //check for edge case where user inputs an income that is < 0
    if (income < 0) {
        return {
            elibible: false,
            message: `Error: Annual income must be greater than or equal to 0. Your input: ${income}`,
        };
    }

    //check if income is <= the state's income threshold for that householdsize
    if (income <= stateIncomeThresholds[state][householdSizeCopy]) {
        return {
            eligible: true,
            message: "You are eligible for a solar loan.",
        };
        //if income is greater than the state's income threshold for that householdsize
    } else {
        return {
            elibible: false,
            message: `You are ineligible for a solar loan. In the state of ${stateIncomeThresholds[state]["stateName"]}, any household size of ${householdSize} cannot exceed an annual income of $${stateIncomeThresholds[state][householdSizeCopy]}.00.`,
        };
    }
}
