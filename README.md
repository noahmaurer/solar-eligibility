# solar-eligibility

## Code Explanation

Noah Maurer
Capital Good Fund - Solar Eligibility Coding Test
Code Explanation

In selecting the state variable input, I chose to use the two-letter abbreviation of the respective state in the form of a string. This approach makes the code more concise and mitigates the likelihood of any spelling errors. On the front end, we could have a select dropdown that lists all of the available states. When the input gets passed to the backend, it would pass in the two-letter abbreviation. While I assume that the inserted data will be a two letter string and one of the given states, I will include edge case checks to make sure that the input value matches the criteria.

For household size, I chose to use an integer. This integer will always be greater than 1, since there would not be zero people applying for solar panel installation. On the front end, we could implement an input that accepts only integers and they must be greater than 0. While I assume that the inserted data will be greater than 0. I will include edge case checks to make sure that the input value matches the criteria.

For the income variable, I chose to assume the variable would be a float because you want to have decimal places for income. On the front end, we could have the user input a number that still shows the number as a traditional currency value, like $100,000, but when the data gets sent, it is sent as 100000.00. This float would be greater than or equal to zero. While it is unlikely that someone would have 0 income, it is possible. While I assume that the inserted data will be a float or integer greater than or equal to 0, I will include edge case checks to make sure that the input value matches the criteria.

To make it easier to check for eligibility criteria, I created an object called stateIncomeThresholds. This object includes keys with the values of the three provided states. Each key has a value of another object. This object includes the stateName, then household size with corresponding AMI threshold limits as their keys.
For example, if you were in the state of Texas and had a household size of 6, a way to check the AMI threshold limit would be: stateIncomeThresholds[TX][6] = 98948.0
This object would likely need to be stored in a separate file for updates, modifications, reusability, etc. For easier access, I have left the data in the same file.

I created a copy of the householdSize to be able to check the edge case where the size is greater than 8. I did this because the object that contains household sizes does not include every possible size. Therefore, if the household size is greater than 8, we only check the threshold that is required for a size of 8 since there are no further instructions on more than 8.

If someone is deemed eligible or ineligible, we return an object that has two keys: eligible, and message. The eligible key has a value that will be true or false. The message key will have a value of a string that indicates a message description.

If there are invalid inputs, there will also be the same return statement, with the eligibility set to false, and the message explaining what went wrong.

The way the return statements are set up, it would be easy to transfer this data to the front end. Example: If someone inputs their data and the return object eligible key is equal to true, we could output the return statement message of "You are eligible for a solar loan” to the user interface.
